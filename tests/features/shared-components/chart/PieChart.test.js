import React from 'react';
import { shallow } from 'enzyme';
import { PieChart } from '../../../../src/features/shared-components';
import configStore from '../../../../src/common/configStore';

describe('shared-components/PieChart', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const store = configStore();
    const renderedComponent = shallow(<PieChart store={store} />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});
