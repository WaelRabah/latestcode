import React from 'react';
import { shallow } from 'enzyme';
import { TagsInput } from '../../../../src/features/shared-components';

describe('shared-components/TagsInput', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<TagsInput />);
    expect(renderedComponent.find('.tags-input').length).toBe(1);
  });
});
