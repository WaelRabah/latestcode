import React from 'react';
import { shallow } from 'enzyme';
import { Modal } from '../../../../src/features/shared-components';

describe('shared-components/Modal', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<Modal />);
    expect(renderedComponent.find('.modal-global-container').length).toBe(1);
  });
});
