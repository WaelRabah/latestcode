import React from 'react';
import { shallow } from 'enzyme';
import { Sources } from '../../../src/features/sources/Sources';

describe('sources/Sources', () => {
  it('renders node with correct class name', () => {
    const props = {
      sources: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <Sources {...props} />
    );

    expect(
      renderedComponent.find('.sources-container').length
    ).toBe(1);
  });
});
