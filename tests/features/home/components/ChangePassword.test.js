import React from 'react';
import { shallow } from 'enzyme';
import ChangePassword  from '../../../../src/features/home/components/ChangePassword';

describe('home/components/ChangePassword', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const props = {
        setIsChangePassword: function() {},
        show: false
    };
    const renderedComponent = shallow(<ChangePassword {...props} />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});
