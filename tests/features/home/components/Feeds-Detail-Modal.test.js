import React from 'react';
import { shallow } from 'enzyme';
import FeedsDetailModal  from '../../../../src/features/home/components/Feeds-Detail-Modal';

describe('home/components/FeedsDetailModal', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const renderedComponent = shallow(<FeedsDetailModal />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});