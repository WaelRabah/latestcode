import React from 'react';
import { AppContainer } from 'react-hot-loader';
import { render } from 'react-dom';
import configStore from './common/configStore';
import routeConfig from './common/routeConfig';
import Root from './Root';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Firebase, { FirebaseContext } from './Firebase';

import { CookiesProvider } from 'react-cookie';
const store = configStore();

function renderApp(app) {
  render(
    <CookiesProvider>
      <FirebaseContext.Provider value={new Firebase()}>
        <AppContainer>{app}</AppContainer>
      </FirebaseContext.Provider>
    </CookiesProvider>,
    document.getElementById('root'),
  );
}

renderApp(<Root store={store} routeConfig={routeConfig} />);

if (module.hot) {
  module.hot.accept('./common/routeConfig', () => {
    const nextRouteConfig = require('./common/routeConfig').default;
    renderApp(<Root store={store} routeConfig={nextRouteConfig} />);
  });
  module.hot.accept('./Root', () => {
    const nextRoot = require('./Root').default;
    renderApp(<Root store={store} routeConfig={nextRoot} />);
  });
}
