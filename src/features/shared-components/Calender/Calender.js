import React, { useState, useEffect } from 'react';

import DateRangePicker from 'react-bootstrap-daterangepicker';
import moment from 'moment';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-daterangepicker/daterangepicker.css';

const ranges = {
  Today: [moment(), moment()],
  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  'Last 30 Days': [moment().subtract(30, 'days'), moment()],
  'Last Year': [
    moment()
      .subtract(1, 'years')
      .startOf('years'),
    moment()
      .subtract(1, 'years')
      .endOf('years'),
  ],
};

const Calender = props => {
  const [date, setDate] = useState(
    `${moment()
      .subtract(6, 'days')
      .format('YYYY-MM-DD')} - ${moment().format('YYYY-MM-DD')}`,
  );
  const [isActive, setActive] = useState(false);
  const [defaultDate, setDefaultDate] = useState({
    startDate: moment().subtract(6, 'days'),
    endDate: moment(),
  });

  useEffect(() => {
    const el = document.getElementsByClassName('daterangepicker');
    if (el.length) {
      el[0].classList.add('custom-date-picker');
      el[0].addEventListener('click', clearOffdays);
    }
    const filterDate = localStorage.getItem('dashboardFilter');
    if (filterDate && JSON.parse(filterDate)['dateRangeFilterValue']) {
      setDate(
        `${JSON.parse(filterDate)['dateRangeFilterValue'].fromDate}-${
          JSON.parse(filterDate)['dateRangeFilterValue'].toDate
        }`,
      );
      setDefaultDate({
        startDate: JSON.parse(filterDate)['dateRangeFilterValue'].fromDate,
        endDate: JSON.parse(filterDate)['dateRangeFilterValue'].toDate,
      });
    }
  }, []);

  function handleEvent(event, picker) {
    event.type === 'show' ? setActive(true) : setActive(false);
    const start = moment(picker.startDate).format('YYYY-MM-DD');
    const end = moment(picker.endDate).format('YYYY-MM-DD');
    setDefaultDate({
      startDate: start,
      endDate: end,
    });
    setDate(`${start} - ${end}`);
    props.setDateRange({ fromDate: start, toDate: end });
  }

  function clearOffdays() {
    const el = document.getElementsByTagName('tbody');
    for (var i = 0; i < el.length; i++) {
      for (var j = 0; j < el[i].rows.length; j++) {
        var result = true;
        for (var k = 0; k < el[i].rows[j].cells.length; k++) {
          if (!el[i].rows[j].cells[k].classList.contains('off')) {
            result = false;
          }
        }
        if (result) {
          el[i].rows[j].remove();
        }
      }
    }
  }
  return (
    <DateRangePicker
      opens="right"
      ranges={ranges}
      alwaysShowCalendars={true}
      linkedCalendars={true}
      autoApply={true}
      startDate={moment(defaultDate.startDate).format("MM/DD/YYYY")}
      endDate={moment(defaultDate.endDate).format("MM/DD/YYYY")}
      locale={{ customRangeLabel: 'Custom' }}
      onApply={(event, value) => handleEvent(event, value)}
      onShow={() => clearOffdays()}
      onShowCalendar={() => clearOffdays()}
    >
      <div className={`input-container ${isActive ? 'active' : ''}`}>
        <i className="la la-calendar icon" />
        <input type="text" placeholder={date} readOnly defaultValue=""></input>
      </div>
    </DateRangePicker>
  );
};

export default Calender;
