import React, { useEffect } from 'react';
import Modal from '../Modal/Modal';
const Button = ({ label, icon, onClick }) => {
  return (
    <button className="btn-default" type="button" onClick={() => onClick()}>
      <i className={`la la-${icon}`} />
      <label>{label}</label>
    </button>
  );
};

const SearchInput = () => {
  return (
    <React.Fragment>
      <div className="search-container">
        <input type="text" placeholder="Search" />
        <i className="la la-search" />
      </div>
    </React.Fragment>
  );
};

const DeleteConfirmation = ({
  confirmDelete,
  show,
  toggleModal,
  title,
  message,
  disabled = false,
}) => {
  useEffect(() => {
    document.body.classList.add('modal-open');
    return () => {
      document.body.classList.remove('modal-open');
    };
  }, [show]);

  return (
    <Modal handleClick={() => toggleModal(false)}>
      <div className={`modal delete-confirmation ${show ? 'modal-show' : 'modal-hide'}`}>
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">{title}</h4>
              <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
            </div>
            <div className="modal-body">
              <div className="confirm-message">
                <p dangerouslySetInnerHTML={{ __html: message }} />
              </div>
            </div>
            <div className="modal-footer">
              <div className={`manage-action-button ${disabled ? 'disable' : ''}`}>
                <button className="btn" onClick={() => toggleModal(false)}>
                  CANCEL
                </button>
                <button className="btn btn-delete" onClick={() => confirmDelete()}>
                  DELETE
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export { Button, SearchInput, DeleteConfirmation };
