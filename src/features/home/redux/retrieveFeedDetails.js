import {
  RETRIEVE_FEED_DETAILS_BEGIN,
  RETRIEVE_FEED_DETAILS_SUCCESS,
  RETRIEVE_FEED_DETAILS_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveFeedDetails(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_FEED_DETAILS_BEGIN,
      data: props.pageId,
    });

    const url = `${apiEndPoints.feeds.RETRIEVE_FEED_DETAILS}?feedId=${props.feedId}&sentiment=${props.sentiment}&pageLimit=${props.pageLimit}&pageId=${props.pageId}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_FEED_DETAILS_SUCCESS,
            data: { data: res.data.data, pageId: props.pageId },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_FEED_DETAILS_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_FEED_DETAILS_BEGIN:
      return {
        ...state,
        retrieveFeedsDetailsBegin: true,
        retrieveFeedsDetailsSuccess: false,
        retrieveFeedsDetailsFailure: false,
        feedDetails: action.data === 1 ? {} : state.feedDetails,
      };
    case RETRIEVE_FEED_DETAILS_SUCCESS:
      const { feedDetails } = state;
      const updatedFeedDetails =
        action.data.pageId > 1
          ? [...feedDetails.data, ...action.data.data.data]
          : action.data.data.data;
      return {
        ...state,
        retrieveFeedsDetailsBegin: false,
        retrieveFeedsDetailsSuccess: true,
        feedDetails: { data: updatedFeedDetails, count: action.data.data.count },
      };
    case RETRIEVE_FEED_DETAILS_FAILURE:
      return {
        ...state,
        retrieveFeedsDetailsBegin: false,
        retrieveFeedsDetailsFailure: true,
      };
    default:
      return state;
  }
}
