import {
  RETRIEVE_COMPARE_BEGIN,
  RETRIEVE_COMPARE_SUCCESS,
  RETRIEVE_COMPARE_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveCompare(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_COMPARE_BEGIN,
    });
    const url = `${apiEndPoints.compare.RETRIEVE_COMPARE}?topicId=${props.topicId}&sources=${props.sources}&fromDate=${props.fromDate}&toDate=${props.toDate}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_COMPARE_SUCCESS,
            data: {
              data: res.data.data,
              topicId: props.topicId,
              topicIndex: props.topicIndex,
              selectedTopicName: props.selectedTopicName,
            },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_COMPARE_FAILURE,
            data: { topicIndex: props.topicIndex, data: {} },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_COMPARE_BEGIN:
      return {
        ...state,
        retrieve_compare_begin: true,
        retrieve_compare_success: false,
        retrieve_compare_error: false,
      };
    case RETRIEVE_COMPARE_SUCCESS:
      const selectedtopic = state.headerTopicData;
      if (parseInt(selectedtopic.topicId) === parseInt(action.data.topicId)) {
        state.activeTopicCompareData = action.data.data;
      } else {
        state.topicCompareData[action.data.topicIndex] = action.data.data;
      }
      return {
        ...state,
        retrieve_compare_begin: false,
        retrieve_compare_success: true,
        selectedTopicName: action.data.selectedTopicName,
      };

    case RETRIEVE_COMPARE_FAILURE:
      state.topicCompareData[action.data.topicIndex] = action.data.data;

      return {
        ...state,
        retrieve_compare_begin: false,
        retrieve_compare_error: true,
      };

    default:
      return state;
  }
}
