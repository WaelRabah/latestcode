import {
  RETRIEVE_CHARTS_BEGIN,
  RETRIEVE_CHARTS_SUCCESS,
  RETRIEVE_CHARTS_FAILURE,
  UPDATE_SELECTED_CHARTS_BEGIN,
  UPDATE_SELECTED_CHARTS_SUCCESS,
  UPDATE_SELECTED_CHARTS_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';
export function retrieveAllCharts(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_CHARTS_BEGIN,
    });
    const url = `${apiEndPoints.charts.RETRIEVE_CHARTS}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_CHARTS_SUCCESS,
            data: res.data.status === 200 ? res.data.data : [],
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_CHARTS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function updateSelectedChartsList(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_SELECTED_CHARTS_BEGIN,
    });
    const url = `${apiEndPoints.charts.UPDATE_SELECTED_CHARTS}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url, props);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_SELECTED_CHARTS_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_SELECTED_CHARTS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_CHARTS_BEGIN:
      return {
        ...state,
        retrieveChartBegin: true,
      };
    case RETRIEVE_CHARTS_SUCCESS:
      return {
        ...state,
        retrieveChartBegin: false,
        availableCharts: action.data,
      };
    case RETRIEVE_CHARTS_FAILURE:
      return {
        ...state,
        retrieveChartBegin: false,
        retrieveChartError: true,
        availableCharts: [],
      };
    case UPDATE_SELECTED_CHARTS_BEGIN:
      return {
        ...state,
        updatedChartId: [],
        updateChartBegin: true,
        updateChartSuccess: false,
        updateChartFailure: false,
      };
    case UPDATE_SELECTED_CHARTS_SUCCESS:
      const updatedChart = action.data.map(chart => chart.chartId);
      const updatedAvailableChart = state.availableCharts.map(chart => {
        if (updatedChart.indexOf(chart.chartId) !== -1) {
          chart.status = !chart.status;
        }
        return chart;
      });
      return {
        ...state,
        updatedChartId: action.data,
        availableCharts: updatedAvailableChart,
        updateChartBegin: false,
        updateChartSuccess: true,
      };
    case UPDATE_SELECTED_CHARTS_FAILURE:
      return {
        ...state,
        updateChartBegin: false,
        updateChartFailure: true,
      };

    default:
      return state;
  }
}
