import { RETRIEVE_CSV_BEGIN, RETRIEVE_CSV_SUCCESS, RETRIEVE_CSV_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveCsv(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_CSV_BEGIN,
    });
    const url = `${apiEndPoints.csv.RETRIEVE_CSV}?fromDate=${props.fromDate}&toDate=${
      props.toDate
    }&topicId=${props.topicId}&sources=${props.sources}&type=${props.type}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          const data = res.data.data;
          dispatch({
            type: RETRIEVE_CSV_SUCCESS,
            data: data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_CSV_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_CSV_BEGIN:
      return {
        ...state,
        retrieveCsvBegin: true,
        retrieveCsvSuccess: false,
        retrieveCsvError: false,
        csvData: '',
      };
    case RETRIEVE_CSV_SUCCESS:
      return {
        ...state,
        retrieveCsvBegin: false,
        retrieveCsvSuccess: true,
        csvData: action.data,
      };

    case RETRIEVE_CSV_FAILURE:
      return {
        ...state,
        retrieveCsvBegin: false,
        retrieveCsvError: true,
      };

    default:
      return state;
  }
}
