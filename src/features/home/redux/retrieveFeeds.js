import {
  RETRIEVE_FEEDS_BEGIN,
  RETRIEVE_FEEDS_SUCCESS,
  RETRIEVE_FEEDS_FAILURE,
  FILTER_RETRIEVE_FEEDS_BEGIN,
  FILTER_RETRIEVE_FEEDS_SUCCESS,
  FILTER_RETRIEVE_FEEDS_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveAllFeeds(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_FEEDS_BEGIN,
    });
    const url = `${apiEndPoints.feeds.RETRIEVE_FEEDS}?fromDate=${props.fromDate}&toDate=${
      props.toDate
    }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sources=${
      props.sources && props.sources.length ? props.sources.join(',').toLowerCase() : 'all sources'
    }`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_FEEDS_SUCCESS,
            data: res.data.status === 200 ? res.data : { data: [], count: null },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_FEEDS_FAILURE,
            data: {},
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}
export function filterAllFeeds(props = {}) {
  return dispatch => {
    dispatch({
      type: FILTER_RETRIEVE_FEEDS_BEGIN,
    });
    const url = `${apiEndPoints.feeds.RETRIEVE_FEEDS}?fromDate=${props.fromDate}&toDate=${
      props.toDate
    }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sources=${
      props.sources && props.sources.length ? props.sources.join(',').toLowerCase() : 'all sources'
    }`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: FILTER_RETRIEVE_FEEDS_SUCCESS,
            data: res.data.status === 200 ? res.data : { data: {}, count: null },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: FILTER_RETRIEVE_FEEDS_FAILURE,
            data: {},
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_FEEDS_BEGIN:
      return {
        ...state,
        retrieveFeedsBegin: true,
        retrieveFeedsSuccess: false,
        retrieveFeedsFailure: false,
      };
    case RETRIEVE_FEEDS_SUCCESS:
      let updatedFeedsData = [];
      if (state.allFeeds.data && action.data.data) {
        updatedFeedsData = [...state.allFeeds.data, ...action.data.data];
      } else if (action.data.data) {
        updatedFeedsData = action.data.data;
      }
      return {
        ...state,
        retrieveFeedsBegin: false,
        retrieveFeedsSuccess: true,
        allFeeds: { data: updatedFeedsData, count: action.data.count },
      };
    case RETRIEVE_FEEDS_FAILURE:
      return {
        ...state,
        retrieveFeedsBegin: false,
        retrieveFeedsFailure: true,
      };
    case FILTER_RETRIEVE_FEEDS_BEGIN:
      return {
        ...state,
        retrieveFeedsBegin: true,
        retrieveFeedsSuccess: false,
        retrieveFeedsFailure: false,
      };
    case FILTER_RETRIEVE_FEEDS_SUCCESS:
      return {
        ...state,

        retrieveFeedsBegin: false,
        retrieveFeedsSuccess: true,
        allFeeds: { data: action.data.data, count: action.data.count },
      };
    case FILTER_RETRIEVE_FEEDS_FAILURE:
      return {
        ...state,
        retrieveFeedsBegin: false,
        retrieveFeedsFailure: true,
      };
    default:
      return state;
  }
}
