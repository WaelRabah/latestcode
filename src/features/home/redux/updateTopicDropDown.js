import {
  UPDATE_TOPICSDROPDOWN_BEGIN,
  UPDATE_TOPICSDROPDOWN_SUCCESS,
  UPDATE_TOPICSDROPDOWN_FAILURE,
  UPDATE_TOPICSDROPDOWN_ON_ADD_TOPIC,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateTopicsDropdown(props = {}) {
  const { updatedTopic, prevTopic } = props;
  return dispatch => {
    dispatch({
      type: UPDATE_TOPICSDROPDOWN_BEGIN,
      data: updatedTopic,
    });
    const url = `${apiEndPoints.topicsDropDown.UPDATE_TOPICDROPDOWN}?topicId=${updatedTopic.topicId}&status=${updatedTopic.status}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_TOPICSDROPDOWN_SUCCESS,
            data: updatedTopic,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_TOPICSDROPDOWN_FAILURE,
            data: prevTopic,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function updateTopicDropdownOnAddTopic(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_TOPICSDROPDOWN_ON_ADD_TOPIC,
      data: props,
    });
  };
}

export function reducer(state, action) {
  let headerTopic = state.headerTopicData;
  switch (action.type) {
    case UPDATE_TOPICSDROPDOWN_BEGIN:
      return {
        ...state,
        updateTopicsdropdownBegin: true,
        updateTopicsdropdownSuccess: false,
        updateTopicsdropdownError: false,
        headerTopicData: action.data,
      };
    case UPDATE_TOPICSDROPDOWN_SUCCESS:
      return {
        ...state,
        updateTopicsdropdownBegin: false,
        updateTopicsdropdownSuccess: true,
        activeTopic: [action.data.topicId],
        headerTopicData: action.data,
      };
    case UPDATE_TOPICSDROPDOWN_FAILURE:
      return {
        ...state,
        updateTopicsdropdownBegin: false,
        updateTopicsdropdownError: true,
        headerTopicData: action.data,
      };
    case UPDATE_TOPICSDROPDOWN_ON_ADD_TOPIC:
      let { topicDropdown } = state;
      topicDropdown.splice(3, 1);
      topicDropdown.push(action.data);
      return {
        ...state,
        topicDropdown,
      };
    default:
      return state;
  }
}
