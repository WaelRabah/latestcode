import React, { Component } from 'react';
import { withFirebase } from '../../../Firebase';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { Modal } from '../../shared-components';
import { toast } from 'react-toastify';

const initialState = {
  password: '',
  newPassword: '',
  confirmPassword: '',
};
const emailRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
class ChangePasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: {},
      formFieldType: 'password',
      formFieldNew: 'password',
      formFieldConfirm: 'password',
      passwordData: JSON.parse(JSON.stringify(initialState)),
      isPasswordChanging: false,
    };
  }

  validatePassword = () => {
    let { error, passwordData } = this.state;

    if (!emailRegex.test(passwordData.newPassword)) {
      error['newPassword'] = true;
      this.setState({ error });
    }
    if (passwordData.newPassword && passwordData.newPassword !== passwordData.confirmPassword) {
      error['confirmPassword'] = true;
      this.setState({ error });
    }
  };

  setFormValues = event => {
    let { passwordData, error } = this.state;
    passwordData[event.target.name] = event.target.value;
    delete error[event.target.name];
    this.setState({
      passwordData,
      error,
    });
  };

  validateForm = form => {
    if (
      !form.password ||
      !form.newPassword ||
      !form.confirmPassword ||
      Object.keys(this.state.error).length
    ) {
      this.setState({
        isPasswordChanging: false,
      });
      return false;
    } else {
      return true;
    }
  };

  ChangePassword = () => {
    const { passwordData } = this.state;
    this.setState({
      isPasswordChanging: true,
    });
    if (!this.validateForm(passwordData)) return false;
    const currentUserEmail = this.props.firebase.auth.currentUser.email;
    this.props.firebase
      .reAuthtenticate(currentUserEmail, passwordData.password)
      .then(() => {
        this.props.firebase
          .changePassword(passwordData.newPassword)
          .then(() => {
            toast.success('Password changed successfully.');
            this.setState(
              {
                isPasswordChanging: false,
              },
              () => {
                this.props.setIsChangePassword(false);
              },
            );
          })
          .catch(err => {
            toast.error(err.code);
            this.setState({
              isPasswordChanging: false,
            });
          });
      })
      .catch(err => {
        toast.error('Old password incorrect.');
        this.setState({
          isPasswordChanging: false,
        });
      });
  };

  componentDidMount() {
    document.body.classList.add('modal-open');
  }

  componentWillUnmount() {
    document.body.classList.add('modal-open');
  }

  render() {
    const { setIsChangePassword, show } = this.props;
    const {
      passwordData,
      error,
      formFieldType,
      formFieldNew,
      formFieldConfirm,
      isPasswordChanging,
    } = this.state;

    return (
      <Modal handleClick={() => setIsChangePassword(false)}>
        <div className={`modal chart-selector ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog change-password">
            <div className="modal-content">
              <div className="modal-header ">
                <h4 className="modal-title">Change Password</h4>

                <i className="la la-close modal-close" onClick={() => setIsChangePassword(false)} />
              </div>
              <div className="modal-body">
                <form>
                  <div className="form-group d-flex flex-row justify-content-between margin-40">
                    <label className="col-form-label">Old Password</label>
                    <div className="position-relative">
                      <input
                        type={formFieldType}
                        className="form-control"
                        name="password"
                        onChange={e => this.setFormValues(e)}
                        defaultValue={passwordData.password}
                      />
                      <i
                        className={`la ${formFieldType === 'password' ? 'la-eye' : 'la-eye-slash'}`}
                        onClick={() =>
                          this.setState(prevState => {
                            return {
                              formFieldType:
                                prevState.formFieldType === 'password' ? 'text' : 'password',
                            };
                          })
                        }
                      ></i>
                    </div>
                  </div>
                  <div className="form-group d-flex flex-row justify-content-between margin-20">
                    <label className="col-form-label">New Password</label>
                    <div className="position-relative">
                      <input
                        type={formFieldNew}
                        className={`form-control input ${
                          error && error.newPassword ? 'input-error' : ''
                        }`}
                        onBlur={this.validatePassword}
                        defaultValue={passwordData.newPassword}
                        name="newPassword"
                        onChange={e => this.setFormValues(e)}
                      />
                      {error.newPassword}
                      {error && error.newPassword && (
                        <span className="instruction-msg error"></span>
                      )}
                      <i
                        className={`la ${formFieldNew === 'password' ? 'la-eye' : 'la-eye-slash'}`}
                        onClick={() =>
                          this.setState(prevState => {
                            return {
                              formFieldNew:
                                prevState.formFieldNew === 'password' ? 'text' : 'password',
                            };
                          })
                        }
                      ></i>

                      <p className={`message ${error && error.newPassword ? 'msg-error' : ''}`}>
                        Password must be at least 8 characters long. <br></br>Password must include
                        1 uppercase letter, 1 lowercase letter, and 1 digit.
                      </p>
                    </div>
                  </div>
                  <div className="form-group d-flex flex-row justify-content-between">
                    <label className="col-form-label">Re-enter New Password</label>
                    <div className="position-relative">
                      <input
                        type={formFieldConfirm}
                        className={`form-control input ${
                          error && error.confirmPassword ? 'input-error' : ''
                        }`}
                        onBlur={this.validatePassword}
                        defaultValue={passwordData.confirmPassword}
                        name="confirmPassword"
                        onChange={e => this.setFormValues(e)}
                      />
                      {error.confirmPassword}
                      {error && error.confirmPassword && (
                        <span className="message msg-error"> Password mismatch</span>
                      )}

                      <i
                        className={`la ${
                          formFieldConfirm === 'password' ? 'la-eye' : 'la-eye-slash'
                        }`}
                        onClick={() =>
                          this.setState(prevState => {
                            return {
                              formFieldConfirm:
                                prevState.formFieldConfirm === 'password' ? 'text' : 'password',
                            };
                          })
                        }
                      ></i>
                    </div>
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <div className="manage-password">
                  <button
                    className={`btn ${isPasswordChanging && 'disabled'}`}
                    onClick={() => setIsChangePassword(false)}
                  >
                    CANCEL
                  </button>

                  <button
                    type="button"
                    className={`btn change-btn ${isPasswordChanging && 'disabled'}`}
                    onClick={() => this.ChangePassword()}
                  >
                    {isPasswordChanging && <i className="la la-spinner la-spin" />} CHANGE PASSWORD
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

const ChangePassword = compose(withRouter, withFirebase)(ChangePasswordForm);
export default ChangePassword;
