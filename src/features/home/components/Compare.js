import React, { Component } from 'react';
import Filter from './Filter';
import { Calender } from '../../shared-components';
import AddCategoryTopic from './AddCategoryTopic';
import moment from 'moment';

const tableLabels = {
  totalMentions: 'Total Mentions',
  positiveMentions: 'Positive Mentions',
  negativeMentions: 'Negative Mentions',
  neutralMentions: 'Neutral Mentions',
  sentimentIndex: 'Sentiment Index',
  joyIndex: 'Joy Index',
  angerIndex: 'Anger Index',
  fearIndex: 'Fear Index',
  sadIndex: 'Sad Index',
};
export class Compare extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sourcesFilter: [],
      isAddCategoryTopic: false,
      selectedTopicId: [],
      selectedTopicName: [],
      isNewTopicSelected: false,
      updateTopicIndex: null,
      selectedSource: 'all sources',
      dateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      topicId: [],
    };

    this.setDateFromDashboardIfAvailable()

    const el = document.getElementsByClassName('daterangepicker');
    if (el.length) {
      el[0].classList.remove('custom-date-picker');
      el[0].classList.add('compare-date-picker');
    }
  }

  setDateFromDashboardIfAvailable = () => {
    const filterDate = localStorage.getItem('dashboardFilter');
    if (filterDate && JSON.parse(filterDate)['dateRangeFilterValue']) {
      const fromDate = JSON.parse(filterDate)['dateRangeFilterValue'].fromDate;
      const toDate  =JSON.parse(filterDate)['dateRangeFilterValue'].toDate;
      this.state.dateRange = {
        fromDate: fromDate,
        toDate: toDate
      };
    }
  };

  showCategoryTopic = newTopic => {
    const { topicCompareData } = this.props;
    this.setState(
      prevState => {
        return {
          isNewTopicSelected: newTopic === 'newTopic',
          updateTopicIndex: newTopic !== 'newTopic' ? newTopic : topicCompareData.length,
          isAddCategoryTopic: !prevState.isAddCategoryTopic,
        };
      },
      () => {
        this.props.getCategories();
      },
    );
  };

  getSelectedCompareData = topicId => {
    const {
      selectedTopicId,
      updateTopicIndex,
      dateRange,
      selectedSource,
      selectedTopicName,
    } = this.state;
    if (selectedTopicId.indexOf(topicId) == -1) {
      this.setState({
        selectedTopicId: [...selectedTopicId, topicId],
      });
    }
    this.props.getCompareData({
      topicId: topicId,
      topicIndex: updateTopicIndex,
      dateRange,
      selectedSource,
      selectedTopicName,
    });
  };

  filterBySource = selectedSource => {
    if (selectedSource.length) {
      this.setState({ selectedSource }, () => {
        let filter = localStorage.getItem('dashboardFilter');
        filter =
          filter && filter.length
            ? { ...JSON.parse(filter), ...{ source: this.state.selectedSource } }
            : { source: this.state.selectedSource };
        localStorage.setItem('dashboardFilter', JSON.stringify(filter));
        this.getUpdatedCompareDataFromFilter();
      });
    } else {
      let filter = localStorage.getItem('dashboardFilter');
      if (filter) {
        filter = JSON.parse(filter);
        if (filter.source) {
          delete filter.source;
        }
        localStorage.setItem('dashboardFilter', JSON.stringify(filter));
      }
    }
  };
  setDateRangeFilter = dateRange => {
    this.setState(
      {
        dateRange,
      },
      () => {
        this.getUpdatedCompareDataFromFilter();
      },
    );
  };
  getCompareDataOnComponentRevisit = () => {
    const { selectedTopicName, selectedSource, dateRange } = this.state;
    for (let i = 0; i < selectedTopicName.length; i++) {
      this.props.getCompareData({
        topicId: selectedTopicName[i].topicId,
        topicIndex: i,
        dateRange,
        selectedSource,
        selectedTopicName,
      });
    }
  };
  getUpdatedCompareDataFromFilter = () => {
    const { selectedSource, dateRange } = this.state;
    this.props.getCompareData({
      dateRange,
      selectedSource,
    });
    this.getCompareDataOnComponentRevisit();
  };

  topicSelected = topicName => {
    const { updateTopicIndex, isNewTopicSelected } = this.state;
    if (isNewTopicSelected) {
      this.setState(
        {
          selectedTopicName: [...this.state.selectedTopicName, topicName],
          isNewTopicSelected: false,
        },
        () => {
          this.getSelectedCompareData(topicName.topicId);
        },
      );
    } else {
      let { selectedTopicName } = this.state;
      selectedTopicName[updateTopicIndex] = topicName;
      this.setState(
        {
          selectedTopicName,
        },
        () => {
          this.getSelectedCompareData(topicName.topicId);
        },
      );
    }
  };

  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      newProps.topicId.length &&
      JSON.stringify(newProps.topicId) !== JSON.stringify(this.state.topicId)
    ) {
      this.setState(
        {
          topicId: newProps.topicId,
        },
        () => {
          this.props.getCompareData({
            dateRange: this.state.dateRange,
            selectedSource: this.state.selectedSource,
          });
        },
      );
    }
  }

  componentDidMount() {
    const { dateRange, selectedSource } = this.state;
    const { topicId, selectedTopicNames } = this.props;
    this.setState(
      {
        topicId,
        selectedTopicName: selectedTopicNames.length ? selectedTopicNames : [],
      },
      () => {
        if (this.state.selectedTopicName.length) this.getCompareDataOnComponentRevisit();
        this.props.getCompareData({
          dateRange,
          selectedSource,
        });
      },
    );
  }

  render() {
    const { sourcesFilter, activeTopicCompareData, topicDropdown, topicCompareData } = this.props;
    const { isAddCategoryTopic, selectedTopicName } = this.state;
    const updatedSourcesFilter = JSON.parse(JSON.stringify(sourcesFilter)).map(source => {
      source.checked = true;
      return source;
    });
    const activeTopic = topicDropdown;
    return (
      <div className="compare-container">
        <div className="compare-header">
          {updatedSourcesFilter.length > 0 && (
            <Filter
              sourcesFilter={[...updatedSourcesFilter]}
              filterBySource={e => this.filterBySource(e)}
            />
          )}
          <Calender setDateRange={e => this.setDateRangeFilter(e)} />
        </div>
        <div className="compare-body">
          <div className="mention-table">
            <table className="table">
              <tbody>
                {Object.values(tableLabels).map((label, index) => (
                  <tr key={index}>
                    <td>{label}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <div className="topic-table">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>
                    <p>{Object.keys(activeTopic).length > 0 && activeTopic.topicName}</p>
                  </th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(tableLabels).map((label, index) => (
                  <tr key={index}>
                    <td>
                      {(Object.keys(activeTopicCompareData).length &&
                        activeTopicCompareData[label]) ||
                        0}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>

          {topicCompareData.length > 0 &&
            topicCompareData.map((topicData, index) => (
              <div className="research-table" key={index}>
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th>
                        {selectedTopicName.length > 0 && (
                          <React.Fragment>
                            <p>{selectedTopicName[index].topicName}</p>
                            <i
                              className="la la-chevron-circle-down"
                              onClick={() => this.showCategoryTopic(index)}
                            ></i>
                          </React.Fragment>
                        )}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {Object.keys(tableLabels).map((label, index) => (
                      <tr key={index}>
                        <td>{(Object.keys(topicData).length && topicData[label]) || 0}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            ))}
          {topicCompareData.length < 2 && (
            <div className="select-category">
              <div className="inner-block" onClick={() => this.showCategoryTopic('newTopic')}>
                <div className="inner-text">
                  <i className="la la-plus"></i>
                  Select Category or Topic to Compare
                </div>
              </div>
            </div>
          )}
          {isAddCategoryTopic && (
            <AddCategoryTopic
              show={isAddCategoryTopic}
              allCategories={JSON.parse(JSON.stringify(this.props.categories))}
              toggleModal={e => this.setState({ isAddCategoryTopic: e })}
              topicSelected={e => this.topicSelected(e)}
            />
          )}
        </div>
      </div>
    );
  }
}
export default Compare;
