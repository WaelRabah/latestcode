import React, { useEffect, useState } from 'react';
import { Modal } from '../../shared-components';

const AddChartModal = React.memo(
  ({ isAddChartActive, handleAddChart, availableCharts, updateAvailableChart }) => {
    const [chartTypes, setChartTypes] = useState([]);
    const [selectedCharts, setselectedCharts] = useState([]);

    useEffect(() => {
      isAddChartActive
        ? document.body.classList.add('modal-open')
        : document.body.classList.remove('modal-open');
    }, [isAddChartActive]);

    useEffect(() => {
      setChartTypes(JSON.parse(JSON.stringify(availableCharts)));
      return () => {
        setChartTypes([]);
        document.body.classList.remove('modal-open');
      };
    }, [availableCharts.length]);

    const handleSelected = i => {
      const updatedChartType = chartTypes.map((chartType, index) => {
        setselectedCharts([
          ...selectedCharts,
          ...[{ chartId: chartTypes[i].chartId, status: !chartTypes[i].status }],
        ]);

        return index === i
          ? {
              ...chartType,
              status: !chartType.status,
            }
          : chartType;
      });
      setChartTypes(updatedChartType);
    };

    return (
      <Modal handleClick={() => handleAddChart(false)}>
        <div className={`modal chart-selector ${isAddChartActive ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Select Chart</h4>
                <i className="la la-close modal-close" onClick={() => handleAddChart(false)} />
              </div>
              <div className="modal-body chart-select-modal">
                <ul className="add-chart-type">
                  {chartTypes &&
                    chartTypes.map((chartType, index) => (
                      <li
                        onClick={() => handleSelected(index)}
                        className={`${chartType.status ? 'active' : ''}`}
                        key={index}
                      >
                        <i className={`la la-${chartType.icon}`}></i>
                        <p>{chartType.name}</p>
                      </li>
                    ))}
                </ul>
              </div>
              <div className="modal-footer">
                <div className="manage-topics">
                  <button className="btn" onClick={() => handleAddChart(false)}>
                    CANCEL
                  </button>
                  <button
                    className="btn btn-submit"
                    onClick={() => updateAvailableChart(selectedCharts)}
                  >
                    SAVE
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  },
);

export default AddChartModal;
