import React, { Component } from 'react';
import { log } from 'util';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      socialMedias: [],
      news: [],
      allSocialMedia: true,
      allNews: true,
      allSources: true,
      allFilter: [],
      isFilterListActive: false,
    };
    this.wrapperRef = React.createRef();
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      newProps.sourcesFilter.length &&
      JSON.stringify(this.state.allFilter) !== JSON.stringify(newProps.sourcesFilter)
    ) {
      this.setState({
        allFilter: JSON.parse(JSON.stringify(newProps.sourcesFilter)),
      });
      const socialMedias = newProps.sourcesFilter.filter(
        source => source.sourceType.toLowerCase() === 'social media',
      );
      const news = newProps.sourcesFilter.filter(
        source => source.sourceType.toLowerCase() === 'news',
      );
      this.setState({
        socialMedias,
        news,
      });
    }
  }

  handleClickOutside = event => {
    if (event && this.wrapperRef.current && !this.wrapperRef.current.contains(event.target)) {
      this.setState({
        isFilterListActive: false,
      });
    }
  };

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside());
  }
  checkAllSources = event => {
    const { socialMedias, news } = this.state;
    const updatedSocialMedias = socialMedias.map(media => {
      media.checked = event.target.checked;
      return media;
    });
    const updatedNews = news.map(source => {
      source.checked = event.target.checked;
      return source;
    });
    this.setState(
      {
        socialMedias: [],
        news: [],
        allSocialMedia: event.target.checked,
        allNews: event.target.checked,
        allSources: event.target.checked,
      },
      () => {
        this.setState({
          socialMedias: updatedSocialMedias,
          news: updatedNews,
        });
      },
    );
    if (event.target.checked) {
      this.props.filterBySource(['all news', 'all social media']);
    } else {
      this.props.filterBySource('');
    }
  };

  filterBySources = (mediaType, source) => {
    const { socialMedias, news } = this.state;
    if (mediaType === 'socialMedias') {
      let allChecked = true;
      const updatedSocialMedias = socialMedias.map(media => {
        if (media.sourceName === source.sourceName) {
          media.checked = !media.checked;
        }
        if (!media.checked) {
          allChecked = false;
        }
        return media;
      });
      this.setState({ socialMedias: updatedSocialMedias, allSocialMedia: allChecked }, () => {
        if (!allChecked) {
          this.setState({
            allSources: false,
          });
        }
      });
    } else {
      let allChecked = true;
      const updatedNews = news.map(_news => {
        if (_news.sourceName === source.sourceName) {
          _news.checked = !_news.checked;
        }
        if (!_news.checked) {
          allChecked = false;
        }
        return _news;
      });
      this.setState({ news: updatedNews, allNews: allChecked }, () => {
        if (!allChecked) {
          this.setState({
            allSources: false,
          });
        }
      });
    }

    let allNews = true;
    let selectedNews = [];
    this.state.news.forEach(newsItem => {
      if (!newsItem.checked) {
        allNews = false;
      } else {
        selectedNews.push(newsItem.sourceName);
      }
    });
    let allSocialMedias = true;
    let selectedSocialMedia = [];
    this.state.socialMedias.forEach(source => {
      if (!source.checked) {
        allSocialMedias = false;
      } else {
        selectedSocialMedia.push(source.sourceName);
      }
    });

    let updatedFilter = [];
    allNews && allSocialMedias
      ? (updatedFilter = ['all news', 'all social media'])
      : allNews && !allSocialMedias
      ? (updatedFilter = ['all news', ...selectedSocialMedia])
      : !allNews && !allSocialMedias
      ? (updatedFilter = [...selectedNews, ...selectedSocialMedia])
      : (updatedFilter = [...selectedNews, 'all social media']);

    this.props.filterBySource(updatedFilter);
    this.setState({});
  };

  checkAllSocialMedia = event => {
    this.setState({ socialMedias: [], allSocialMedia: event.target.checked });
    const updatedSocialMedias = this.state.socialMedias.map(media => {
      media.checked = event.target.checked;
      return media;
    });
    if (!event.target.checked) {
      this.setState({
        allSources: false,
      });
    }
    this.setState({ socialMedias: [] }, () => {
      this.setState({
        socialMedias: updatedSocialMedias,
      });
    });
    if (event.target.checked) {
      let selectedNewsFilter = this.state.news.filter(source => source.checked);
      selectedNewsFilter = selectedNewsFilter.map(item => item.sourceName);
      if (this.state.allNews) {
        this.props.filterBySource(['all social media', 'all news']);
      } else {
        this.props.filterBySource(['all social media', ...selectedNewsFilter]);
      }
    } else {
      if (this.state.allNews) {
        let selectedFilters = this.state.socialMedias.filter(source => source.checked);
        selectedFilters = selectedFilters.map(item => item.sourceName);
        this.props.filterBySource(['all news', ...selectedFilters]);
      } else {
        const allSources = [...this.state.socialMedias, ...this.state.news];
        let selectedFilters = allSources.filter(source => source.checked);
        selectedFilters = selectedFilters.map(item => item.sourceName);
        this.props.filterBySource([...selectedFilters]);
      }
    }
  };

  checkAllNews = event => {
    this.setState({ news: [], allNews: event.target.checked });
    const updatedNews = this.state.news.map(media => {
      media.checked = event.target.checked;
      return media;
    });
    if (!event.target.checked) {
      this.setState({
        allSources: false,
      });
    }
    this.setState({ news: [] }, () => {
      this.setState({
        news: updatedNews,
      });
    });
    if (event.target.checked) {
      let selectedNewsFilter = this.state.socialMedias.filter(source => source.checked);
      selectedNewsFilter = selectedNewsFilter.map(item => item.sourceName);
      if (this.state.allSocialMedia) {
        this.props.filterBySource(['all social media', 'all news']);
      } else {
        this.props.filterBySource(['all news', ...selectedNewsFilter]);
      }
    } else {
      if (this.state.allSocialMedia) {
        let selectedFilters = this.state.news.filter(source => source.checked);
        selectedFilters = selectedFilters.map(item => item.sourceName);
        this.props.filterBySource(['all social media', ...selectedFilters]);
      } else {
        const allSources = [...this.state.socialMedias, ...this.state.news];
        let selectedFilters = allSources.filter(source => source.checked);
        selectedFilters = selectedFilters.map(item => item.sourceName);
        this.props.filterBySource([...selectedFilters]);
      }
    }
  };

  render() {
    let {
      isFilterListActive,
      news,
      socialMedias,
      allSocialMedia,
      allNews,
      allSources,
    } = this.state;
    const sourceFilter = localStorage.getItem('dashboardFilter');
    if (sourceFilter) {
      const savedFilters = JSON.parse(sourceFilter)['source'];
      if (savedFilters) {
        socialMedias = socialMedias.map(media => {
          if (
            savedFilters.indexOf(media.sourceName.toLowerCase()) !== -1 ||
            savedFilters.indexOf('all social media') !== -1
          ) {
            media.checked = true;
          } else {
            media.checked = false;
          }
          return media;
        });
        news = news.map(newsItem => {
          if (
            savedFilters.indexOf(newsItem.sourceName.toLowerCase()) !== -1 ||
            savedFilters.indexOf('all news') !== -1
          ) {
            newsItem.checked = true;
          } else {
            newsItem.checked = false;
          }
          return newsItem;
        });
        if (
          savedFilters.indexOf('all news') !== -1 &&
          savedFilters.indexOf('all social media') !== -1
        ) {
          allSources = true;
          allNews = true;
          allSocialMedia = true;
        } else {
          allSocialMedia = false;
          allNews = false;
          allSources = false;
        }
        allNews = savedFilters.indexOf('all news') !== -1 ? true : false;
        allSocialMedia = savedFilters.indexOf('all social media') !== -1 ? true : false;
      }
    }
    return (
      <div className="custom-filter-plugin" ref={this.wrapperRef}>
        <div
          className={`input-container ${isFilterListActive ? 'active' : ''}`}
          onClick={() =>
            this.setState(prevState => {
              return {
                isFilterListActive: !prevState.isFilterListActive,
              };
            })
          }
        >
          <i className="la la-filter icon" />
          <input type="text" placeholder="Sources" readOnly defaultValue=""></input>
        </div>
        <div className={`filter-container ${isFilterListActive ? 'show' : ''}`}>
          <div className="row">
            <div className="col-12">
              <div className="select-item" style={{ width: '100%' }}>
                <input
                  className="styled-checkbox"
                  id="styled-checkbox-1"
                  type="checkbox"
                  value=""
                  readOnly
                  checked={allSocialMedia && allNews ? true : allSources}
                  onChange={e => this.checkAllSources(e)}
                />
                <label htmlFor="styled-checkbox-1">All Sources</label>
              </div>
            </div>
            {socialMedias.length > 0 && (
              <div className="col-6 social-media">
                <div className="select-item">
                  <input
                    className="styled-checkbox"
                    id="all_social_media"
                    type="checkbox"
                    value="all social media"
                    readOnly
                    checked={allSocialMedia}
                    onChange={e => this.checkAllSocialMedia(e)}
                  />
                  <label htmlFor="all_social_media">All Social Media</label>
                </div>
                {socialMedias.map((source, index) => (
                  <div className="select-item" key={'social_' + index}>
                    <input
                      className="styled-checkbox"
                      id={'social_' + index}
                      type="checkbox"
                      readOnly
                      onChange={() => this.filterBySources('socialMedias', source)}
                      defaultChecked={source.checked}
                      value={source.sourceName}
                    />
                    <label htmlFor={'social_' + index}>{source.sourceName}</label>
                  </div>
                ))}
              </div>
            )}
            {news.length > 0 && (
              <div className="col-6 news-block">
                <div className="select-item">
                  <input
                    className="styled-checkbox"
                    id="all_news"
                    type="checkbox"
                    value="all news"
                    readOnly
                    checked={allNews}
                    onChange={e => this.checkAllNews(e)}
                  />
                  <label htmlFor="all_news">All News</label>
                </div>
                {news.map((source, index) => (
                  <div className="select-item" key={index}>
                    <input
                      className="styled-checkbox"
                      id={`source-${index}`}
                      type="checkbox"
                      defaultChecked={source.checked}
                      value={source.sourceName}
                      onChange={() => this.filterBySources('news', source)}
                    />
                    <label htmlFor={`source-${index}`}>{source.sourceName}</label>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Filter;
