import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import Dashboard from './components/Dashboard';
import { chartIcons } from './mock';
import { Compare } from './components/Compare';
import moment from 'moment';
import { toast } from 'react-toastify';
import * as FileSaver from 'file-saver';
import { UNIMPLEMENTED_CHART_IDS } from './redux/constants';

const pageLimit = 10;
export class DefaultPage extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  state = {
    activeTopicCompareData: {},
    topicCompareData: [],
    topicDropdown: {},
    allCategories: [],
    availableCharts: [],
    selectedTopicID: [],
    comments: { data: {}, count: null },
    commentsFeedsFilterDate: {
      fromDate: moment()
        .subtract(6, 'days')
        .format('YYYY-MM-DD'),
      toDate: moment().format('YYYY-MM-DD'),
    },
    currentPage: 1,
    isCommentsLoading: false,
    isFeedsLoading: false,
    isRelevenceUpdating: false,
    isShareReportSending: false,
    sentiments: [],
    sources: [],
    feeds: { data: {}, count: null },
    isChartUpdating: false,
    csvData: '',
    isDownloadingCsv: false,
    csvArticleType: '',
    selectedTopicName: [],
    shareReportStatus:false
  };

  updateSelectedCharts = selectedCharts => {
    const { updateSelectedChartsList } = this.props.actions;
    this.setState(
      {
        isChartUpdating: true,
      },
      () => {
        updateSelectedChartsList(selectedCharts);
      },
    );
  };

  updateFeedsRelevant = feedId => {
    const { updateFeedsRelevant } = this.props.actions;
    this.setState({ isRelevanceUpdating: true });
    updateFeedsRelevant(feedId);
  };

  getCsvData = articleType => {
    this.setState(
      {
        isDownloadingCsv: true,
        csvArticleType: articleType,
      },
      () => {
        const sources = this._getSourcesListFromLocalStorageOrGetDefaultList();
        this.props.actions.retrieveCsv({
          topicId: this.state.selectedTopicID,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sources: sources.length
            ? sources.join(',').toLocaleLowerCase().trim()
            : 'all sources',
          type: articleType == 'comments' ? 'comment' : 'feed',
        });
      },
    );
  };

  loadMoreComments = () => {
    this.setState(
      prevState => {
        return {
          currentPage: prevState.currentPage + 1,
          isCommentsLoading: true,
        };
      },
      () => {
        this.props.actions.retrieveAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };

  getCategoryModalData = () => {
    this.props.actions.retrieveAllCategories();
  };

  getCompareData = (props = {}) => {
    this.props.actions.retrieveCompare({
      topicId: props.topicId ? props.topicId : this.state.selectedTopicID,
      topicIndex: props.topicIndex,
      fromDate: props.dateRange.fromDate,
      toDate: props.dateRange.toDate,
      sources: props.selectedSource,
      selectedTopicName: props.selectedTopicName ? props.selectedTopicName : [],
    });
  };

  loadMoreFeeds = () => {
    this.setState(
      prevState => {
        return {
          currentPage: prevState.currentPage + 1,
          isFeedsLoading: true,
        };
      },
      () => {
        this.props.actions.retrieveAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };
  commentsFeedsFilterByDate = date => {
    this.setState(
      { commentsFeedsFilterDate: date, currentPage: 1, isCommentsLoading: true },
      () => {
        this.props.actions.filterAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
        this.props.actions.filterAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };

  resetFilterInChart = () => {
    this.setState(
      prevState => {
        return {
          currentPage: 1,
          isCommentsLoading: true,
          sentiments: [],
          sources: [],
        };
      },
      () => {
        this.props.actions.filterAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
        this.props.actions.filterAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };

  sendEmailReport = formData => {
    const { createShareReportFile } = this.props.actions;
    this.setState(
      {
        isShareReportSending: true,
      },
      () => {
        createShareReportFile(formData);
      },
    );
  };
  commentsFilter = filterObject => {
    const sources = filterObject.sources 
      ? filterObject.sources 
      : this.state.sources;
    this.setState(
      prevState => {
        return {
          currentPage: 1,
          isCommentsLoading: true,
          [filterObject.key]: filterObject.value,
        };
      },
      () => {
        this.props.actions.filterAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.fromDate,
          toDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: sources
        });
        this.props.actions.filterAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentPage,
          pageLimit: pageLimit,
          fromDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.fromDate,
          toDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: sources
        });
      },
    );
  };

  download = text => {
    const { commentsFeedsFilterDate, csvArticleType } = this.state;
    const filename = `${csvArticleType}_${commentsFeedsFilterDate.fromDate}_${commentsFeedsFilterDate.toDate}.csv`;
    const blob = new Blob([text], { type: 'text/csv' });
    FileSaver.saveAs(blob, filename);
    this.setState({
      csvData: '',
    });
  };

  _getSourcesListFromLocalStorageOrGetDefaultList = () => {
    const savedFilterData = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (savedFilterData && savedFilterData.source) {
      return savedFilterData.source;
    }
    else {
      return [];
    }
  }

  _getSentimentsListFromLocalStorageOrGetDefaultList = () => {
    const savedFilterData = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (savedFilterData && savedFilterData.commentFilterItems && savedFilterData.commentFilterItems.length) {
      return this._convertCommentFilterItemsToArrayOfStrings(savedFilterData.commentFilterItems);
    }
    else {
      return ['all'];
    }
  }

  _convertCommentFilterItemsToArrayOfStrings(commentFilterItems) {
    let sentiments = [];
    for (let i=0; i<commentFilterItems.length; i++) {
      let commentFilterItem = commentFilterItems[i];
      if (commentFilterItem.name === "all" && commentFilterItem.checked) {
        return ['all'];
      }
      else if (commentFilterItem.checked) {
        sentiments.push(commentFilterItem.value);
      }
    }
    return sentiments;
  }

  componentDidMount() {
    const { retrieveAllCharts } = this.props.actions;
    const savedFilterData = JSON.parse(localStorage.getItem('dashboardFilter'));
    const { commentsFeedsFilterDate } = this.state;
    if (savedFilterData && Object.keys(savedFilterData).length) {
      if (savedFilterData.dateRangeFilterValue) {
        commentsFeedsFilterDate['toDate'] = savedFilterData.dateRangeFilterValue.toDate;
        commentsFeedsFilterDate['fromDate'] = savedFilterData.dateRangeFilterValue.fromDate;
      }
      this.setState(
        {
          sources: this._getSourcesListFromLocalStorageOrGetDefaultList(),
          sentiments: this._getSentimentsListFromLocalStorageOrGetDefaultList(),
          commentsFeedsFilterDate,
        },
        () => {
          retrieveAllCharts();
        },
      );
    } else {
      retrieveAllCharts();
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      JSON.stringify(this.state.availableCharts) !==
      JSON.stringify(newProps.home.availableCharts)
    ) {
      const updatedChartData = JSON.parse(JSON.stringify(newProps.home.availableCharts)).map(
        chart => {
          chart['icon'] = chartIcons[chart.chartType.toLowerCase()];
          return chart;
        },
      );

      const filteredAndUpdatedChartData = this._filterOutUnimplementedCharts(updatedChartData);

      this.setState({
        availableCharts: [...filteredAndUpdatedChartData],
      });
    }

    if (newProps.home.updatedChartId.length && this.state.isChartUpdating) {
      const updatedChartList = newProps.home.updatedChartId.map(chart => chart.chartId);
      const trueCount = newProps.home.updatedChartId.filter(chart => chart.status === true);
      if (trueCount.length === updatedChartList.length) {
        toast.success('Chart(s) has been successfully added to the dashboard!');
      } else if (!trueCount.length) {
        toast.success('Chart(s) has been successfully removed from the dashboard!');
      } else {
        toast.success(
          'Chart(s) has been successfully added to and removed from the dashboard!',
        );
      }

      this.setState({
        isChartUpdating: false,
      });
    }

    if (
      newProps.home.activeTopic.length &&
      JSON.stringify(newProps.home.activeTopic) !==
        JSON.stringify(this.state.selectedTopicID)
    ) {
      const selectedTopicID = newProps.home.activeTopic;
      const postData = {
        topicId: selectedTopicID,
        currentPage: this.state.currentPage,
        fromDate: this.state.commentsFeedsFilterDate.fromDate,
        toDate: this.state.commentsFeedsFilterDate.toDate,
        sources: this.state.sources,
        sentiments: this.state.sentiments,
        pageLimit,
      };

      this.props.actions.retrieveAllComments(postData);
      this.props.actions.retrieveAllFeeds(postData);
      this.setState({
        topicDropdown: newProps.home.headerTopicData,
        selectedTopicID,
      });
    }

    if (
      newProps.home.topicDropdown.length &&
      JSON.stringify(newProps.home.headerTopicData) !==
        JSON.stringify(this.state.topicDropdown)
    ) {
      this.setState({
        topicDropdown: newProps.home.headerTopicData,
      });
    }

    if (
      Object.keys(newProps.home.allComments).length &&
      JSON.stringify(this.state.comments) !== JSON.stringify(newProps.home.allComments)
    ) {
      this.setState({
        isCommentsLoading: false,
        comments: newProps.home.allComments,
      });
    }
    if (
      Object.keys(newProps.home.allFeeds).length &&
      JSON.stringify(this.state.feeds) !== JSON.stringify(newProps.home.allFeeds)
    ) {
      this.setState({
        isFeedsLoading: false,
        feeds: newProps.home.allFeeds,
      });
    }

    if (newProps.home.updateCommentsRelevantSuccess && this.state.isRelevanceUpdating) {
      toast.success('Relevance has been updated');
      this.setState({
        isRelevanceUpdating: false,
      });
    }

    if (newProps.home.updateFeedsRelevantSuccess && this.state.isRelevanceUpdating) {
      toast.success('Relevance has been updated');
      this.setState({
        isRelevanceUpdating: false,
      });
    }

    if (newProps.home.updateCommentsRelevantailure && this.state.isRelevanceUpdating) {
      toast.error('Relevance has not been updated');
      this.setState({
        isRelevanceUpdating: false,
      });
    }

    if (newProps.home.sendShareReportSuccess && this.state.isShareReportSending) {
      toast.success('Report shared successfully.');
      this.setState({
        isShareReportSending: false,
      });
    }
    if (newProps.home.sendShareReportError && this.state.isShareReportSending) {
      toast.error('Failed to share report.');
      this.setState({
        isShareReportSending: false,
      });
    }
    if (
      JSON.stringify(newProps.home.activeTopicCompareData) !==
      JSON.stringify(this.state.activeTopicCompareData)
    ) {
      this.setState({
        activeTopicCompareData: newProps.home.activeTopicCompareData,
      });
    }
    if (
      newProps.home.selectedTopicName.length &&
      JSON.stringify(newProps.home.selectedTopicName) !==
        JSON.stringify(this.state.selectedTopicName)
    ) {
      this.setState({
        selectedTopicName: newProps.home.selectedTopicName,
      });
    }
    if (
      newProps.home.topicCompareData.length &&
      JSON.stringify(newProps.home.topicCompareData) !==
        JSON.stringify(this.state.topicCompareData)
    ) {
      this.setState({
        topicCompareData: newProps.home.topicCompareData,
      });

      if (
        (newProps.sendShareReportSuccess || newProps.sendShareReportError) &&
        this.state.isShareReportSending
      ) {
        this.setState({
          isShareReportSending: false,
        });
      }
    }

    if (this.hasPropsForCSVDownload(newProps)) {
      this.componentWillReceivePropsForCSVDownload(newProps);
    }
  }

  hasPropsForCSVDownload(newProps) {
    if (
      newProps.home.csvData !== this.state.csvData &&
      newProps.home.retrieveCsvSuccess &&
      this.state.isDownloadingCsv
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  componentWillReceivePropsForCSVDownload(newProps) {
    this.setState(
      {
        csvData: newProps.home.csvData,
        isDownloadingCsv: false,
      },
      () => {
        if (this.state.csvData) {
          this.download(this.state.csvData);
        }
      },
    );
  }

  _filterOutUnimplementedCharts = (chartData) => {
    return chartData.filter(chart => ! UNIMPLEMENTED_CHART_IDS.includes(chart.chartId));
  }

  render() {
    const {
      comments,
      isCommentsLoading,
      feeds,
      isFeedsLoading,
      selectedTopicID,
      activeTopicCompareData,
      topicCompareData,
      isShareReportSending,
      topicDropdown,
      availableCharts,
      selectedTopicName,
    } = this.state;
    const {
      updateCommentsRelevant,
      retrieveAllCategories,
      retriveShareReportStatus,
      retrieveFeedDetails,
    } = this.props.actions;
    const availableSources = this.props.home.allSources.filter(source => source.selected);
    const { activeTab, shareReportStatus } = this.props.home;
    return (
      <div className="home-default-page">
        {activeTab && activeTab === 'dashboard' ? (
          <Dashboard
            comments={comments}
            feeds={feeds}
            loadMoreComments={() => this.loadMoreComments()}
            commentsFilter={filterObj => this.commentsFilter(filterObj)}
            getCsvData={e => this.getCsvData(e)}
            sourcesFilter={availableSources}
            availableCharts={availableCharts}
            isCommentsLoading={isCommentsLoading}
            loadMoreFeeds={() => this.loadMoreFeeds()}
            filterFeedsCommentsByDate={value => this.commentsFeedsFilterByDate(value)}
            isFeedsLoading={isFeedsLoading}
            topicId={selectedTopicID.length ? [...selectedTopicID] : {}}
            updateCommentsRelevant={e =>
              this.setState({ isRelevanceUpdating: true }, () => {
                updateCommentsRelevant(e);
              })
            }
            headerTopicData={this.props.home.headerTopicData}
            resetFilterInChart={e => this.resetFilterInChart()}
            updateFeedsRelevant={e => this.updateFeedsRelevant(e)}
            {...this.state}
            updateSelectedCharts={e => this.updateSelectedCharts(e)}
            retrieveShareReportMessage={() =>
              retriveShareReportStatus({ topicId: selectedTopicID })
            }
            sendEmailReport={e => this.sendEmailReport(e)}
            isShareReportSending={isShareReportSending}
            shareReportStatus={shareReportStatus}
            retrieveFeedDetails={e => retrieveFeedDetails(e)}
            feedDetails={this.props.home.feedDetails}
            retrieveCategoriesBegin={this.props.home.retrieveCategoriesBegin}
            retrieveFeedsBegin={this.props.home.retrieveFeedsBegin}
          />
        ) : (
          <Compare
            sourcesFilter={availableSources}
            activeTopicCompareData={activeTopicCompareData}
            topicDropdown={topicDropdown}
            topicId={[...selectedTopicID]}
            topicCompareData={topicCompareData}
            categories={this.props.home.allCategories}
            commentsFilter={(value, key) => this.commentsFilter(value, key)}
            getCompareData={e => this.getCompareData(e)}
            selectedTopicNames={selectedTopicName}
            getCategories={e => retrieveAllCategories(e)}
            getCategoryModalData={e => this.getCategoryModalData(e)}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultPage);
