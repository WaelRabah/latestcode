import { Topics } from './';

export default {
  path: '/dashboard/topics',
  name: 'Topics',
  childRoutes: [{ path: 'topics', name: 'Topics', component: Topics, isIndex: true }],
};
