import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Button, DeleteConfirmation, Loader, NoTopic } from '../shared-components';
import { updateTopicDropdownOnAddTopic } from '../home/redux/updateTopicDropDown';
import AddTopic from './components/AddTopic';
import { toast } from 'react-toastify';
import {DebounceInput} from 'react-debounce-input';

export class Topics extends Component {
  static propTypes = {
    topics: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  state = {
    currentPage: 1,
    totalPage: 1,
    currentPageStartIndex: 0,
    currentPageEndIndex: 0,
    filterText: "",
    isAddTopic: false,
    pageLimit: 10,
    retrieveLimit: 10000,
    editTopicData: {},
    addTopicActionType: 'add',
    confirmDeleteModal: false,
    topicToDelete: null,
    allTopicsData: [],
    filteredTopicsData: [],
    paginatedAndFilteredTopicsData: [],
    updatedTopicData: {},
    allCategories: [],
  };

  showAddTopic = () => {
    this.setState(prevState => {
      return {
        isAddTopic: !prevState.isAddTopic,
        addTopicActionType: 'add',
      };
    });
  };

  editTopic = topic => {
    this.setState(
      {
        addTopicActionType: 'edit',
        editTopicData: topic,
      },
      () =>
        this.setState({
          isAddTopic: true,
        }),
    );
  };

  // addEditModalAction = topicData => {
  //   const { createTopics, updateTopics } = this.props.actions;
  //   const { addTopicActionType, editTopicData } = this.state;
  //   if (addTopicActionType === 'add') {
  //     createTopics(topicData);
  //     this.setState({ updatedTopicData: topicData });
  //   } else {
  //     const updatedTopics = Object.assign({}, topicData, { topicId: editTopicData.topicId });
  //     this.setState({ updatedTopicData: updatedTopics });
  //     updateTopics(updatedTopics);
  //   }
  // };

  addEditModalAction = topicData => {
    const { createTopics, updateTopics } = this.props.actions;
    const { addTopicActionType, editTopicData } = this.state;
    if (addTopicActionType === 'add') {
      createTopics(topicData);
      
      this.setState({ updatedTopicData: topicData , isAddTopic : false });
    } else {
      const updatedTopics = Object.assign({}, topicData, { topicId: editTopicData.topicId });
      this.setState({ updatedTopicData: updatedTopics , isAddTopic :false });
      updateTopics(updatedTopics);
    }
  };

  removeTopic = topic => {
    this.setState({ confirmDeleteModal: true, topicToDelete: topic });
  };

  toggleModal = () => {
    this.setState({ confirmDeleteModal: false });
  };

  deleteTopic = () => {
    const { deleteTopic } = this.props.actions;
    const { topicToDelete } = this.state;
    deleteTopic(topicToDelete);
  };

  handlePagination = pageChangeType => {
    const {
      currentPage,
      totalPage,
      pageLimit,
      filteredTopicsData
    } = this.state;
    let newPageNumber = null;
    switch (pageChangeType) {
      case 'next':
        if (currentPage < totalPage) {
          newPageNumber = currentPage + 1;
        }
        break;
      case 'prev':
        if (currentPage > 1) {
          newPageNumber = currentPage - 1;
        }
        break;
      case 'reset':
        newPageNumber = 1;
      case 'same':
        if (currentPage > totalPage) {
          newPageNumber = totalPage;
        }
        else {
          newPageNumber = currentPage;
        }
      default:
        break;
    }

    if (newPageNumber != null) {
      const currentPageStartIndex = (pageLimit*(newPageNumber-1));
      const currentPageEndIndex   = (pageLimit*newPageNumber);
      const paginatedAndFilteredTopicsData = filteredTopicsData.slice(currentPageStartIndex,currentPageEndIndex);
      this.setState({
        currentPage: newPageNumber,
        currentPageStartIndex: currentPageStartIndex,
        currentPageEndIndex: currentPageEndIndex,
        paginatedAndFilteredTopicsData: paginatedAndFilteredTopicsData
      });
    }
  };

  PaginationControl = () => {
    const { currentPage, totalPage } = this.state;
    const { allTopics } = this.props.topics;
    
    return (
      <React.Fragment>
        <div className="total-count">
          <label>
            <strong>{allTopics['count']}</strong> Topics
          </label>
        </div>
        <div className="pagination-controls">
          <span className="prev" onClick={() => this.handlePagination('prev')}></span>
          <label>
            Page{' '}
            <strong>
              {currentPage} of {totalPage}
            </strong>
          </label>
          <span className="next" onClick={() => this.handlePagination('next')}></span>
        </div>
      </React.Fragment>
    );
  };

  componentWillUnmount() {
    this.props.actions.resetTopicState();
  }

  componentDidMount() {
    const { retrieveTopics, retrieveCategories } = this.props.actions;
    const { currentPage, retrieveLimit } = this.state;
    retrieveTopics({ pageId: currentPage, pageLimit: retrieveLimit});
    retrieveCategories();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.topics.allTopics.data && nextProps.topics.allTopics.data.length) {
      this.setState({
        allTopicsData: nextProps.topics.allTopics.data
      }, () => {
        const { filterText } = this.state;
        this._filterTopics({filterText: filterText, keepCurrentPage: true});
      });
    }
    if (
      (nextProps.topics.createTopicsSuccess || nextProps.topics.updateTopicsSuccess) &&
      Object.keys(this.state.updatedTopicData).length
    ) {
      let { allTopicsData, updatedTopicData } = this.state;
      nextProps.topics.createTopicsSuccess
        ? toast.success('Topic has been successfully added!')
        : toast.success('Topic has been successfully updated!');
      let topicIndex = -1;
      allTopicsData.forEach((topic, index) => {
        if (topic.topicId === nextProps.topics.createdTopicId) {
          topicIndex = index;
        }
      });
      if (
        nextProps.topics.createTopicsSuccess &&
        Object.keys(updatedTopicData).length &&
        nextProps.topics.createdTopicId
      ) {
        const newTopicDropDown = {
          topicId: [nextProps.topics.createdTopicId],
          topicName: updatedTopicData.topicName,
          status: false,
        };
        this.props.actions.updateTopicDropdownOnAddTopic(newTopicDropDown);
        allTopicsData.splice(topicIndex !== -1 ? topicIndex : allTopicsData.length - 1, 1);
        updatedTopicData['topicId'] = nextProps.topics.createdTopicId;
        topicIndex !== -1
          ? (allTopicsData[topicIndex] = updatedTopicData)
          : allTopicsData.unshift(updatedTopicData);
        this.setState({ allTopicsData, updatedTopicData: {} });
      }

      this.setState({
        isAddTopic: false,
        updatedTopicData: {},
      });
    }
    if (nextProps.topics.deleteTopicSuccess && this.state.topicToDelete != null) {
      toast.success('Topic has been successfully deleted!');
      const { retrieveTopics } = this.props.actions;
      const { currentPage, retrieveLimit, allTopicsData } = this.state;
      this.setState({ confirmDeleteModal: false, topicToDelete: null, allTopicsData }, () => {
        retrieveTopics({ pageId: currentPage, pageLimit: retrieveLimit });
      });
    }

    if (
      nextProps.topics.retrieveCategoriesSuccess &&
      JSON.stringify(nextProps.topics.allCategories) !== JSON.stringify(this.state.allCategories)
    ) {
      this.setState({
        allCategories: nextProps.topics.allCategories,
      });
    }
  }

  onBlurForSearchInput = e => {
    const filterText = e.target.value;
    this._filterTopics({filterText: filterText, keepCurrentPage: false});
  }

  _filterTopics = ({filterText,keepCurrentPage}) => {
    this.setState({filterText: filterText});
    const { allTopicsData } = this.state;
    const filteredTopicsData = (filterText.trim() == "") ? allTopicsData : allTopicsData.filter(this._topicContainsString);
    this.setState({
      filteredTopicsData: filteredTopicsData,
      totalPage: Math.ceil(filteredTopicsData.length / this.state.pageLimit) || 1,
    }, () => {
      if (keepCurrentPage) {
        this.handlePagination("same");
      }
      else {
        this.handlePagination("reset");
      }
    });
  }

  _topicContainsString = topic => {
    const {filterText} = this.state;
    const lcFilterText = filterText.toLowerCase();
    let containsFilterText = false;
    try {
      if (topic.topicName.toLowerCase().includes(lcFilterText)) {
        return true;
      }
    }
    catch (err) {}
    try {
      if (topic.description.toLowerCase().includes(lcFilterText)) {
        return true;
      }
    }
    catch (err) {}
    topic.categories.forEach(function(category){
      if (category.toLowerCase().includes(lcFilterText)) {
        containsFilterText = true;
      }
    });
    topic.topicKeywords.forEach(function(topicKeyword){
      if (topicKeyword.toLowerCase().includes(lcFilterText)) {
        containsFilterText = true;
      }
    });
    return containsFilterText;
  }

  render() {
    const {
      isAddTopic,
      confirmDeleteModal,
      topicToDelete,
      allTopicsData,
      filteredTopicsData,
      paginatedAndFilteredTopicsData,
      allCategories
    } = this.state;
    const {
      deleteTopicBegin,
      retrieveTopicsBegin,
      createTopicsSuccess,
      createTopicsBegin,
      updateTopicsBegin,
    } = this.props.topics;
    return (
      <div className="topics-container">
        <div className="topics">
          <div className="topics-header">
            <Button
              label="Create Category or Topic"
              icon="plus"
              onClick={() => this.showAddTopic()}
            />
            <div className="pagination">
            <div className="search-container">
              <DebounceInput 
                  minLength={0}
                  onBlur={this.onBlurForSearchInput}
                  onChange={this.onBlurForSearchInput}
                  placeholder="Search"
              />
              <i className="la la-search" />
            </div>
              <this.PaginationControl />
            </div>
          </div>

          <div className="topic-table">
            {retrieveTopicsBegin && <Loader />}
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Topic Name</th>
                  <th>Description</th>
                  <th>Topic Keywords</th>
                  <th>Categories</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {paginatedAndFilteredTopicsData.length > 0 &&
                  paginatedAndFilteredTopicsData.map((topic, index) => (
                    <tr key={topic.topicId}>
                      <td>{topic.topicName}</td>
                      <td>{topic.description}</td>
                      <td>{topic.topicKeywords.join(', ')}</td>
                      <td>{topic.categories.join(', ')}</td>
                      <td>
                        <button className="btn edit" onClick={() => this.editTopic(topic)}>
                          Edit
                        </button>
                        <button className="btn delete" onClick={() => this.removeTopic(topic)}>
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
            {!filteredTopicsData.length && !retrieveTopicsBegin && (
              <NoTopic
                title="Almost There"
                message="Create a category or topic, and start analysing the sentiments and emotions."
                icons={['area-chart', 'pie-chart', 'bar-chart']}
              >
                <button className="btn-add-topic" onClick={() => this.showAddTopic()}>
                  CREATE CATEGORY OR TOPIC
                </button>
              </NoTopic>
            )}
          </div>

          <div className="table-footer">
            <div className="pagination">
              <this.PaginationControl />
            </div>
          </div>
        </div>
        {isAddTopic && (
          <AddTopic
            show={isAddTopic}
            toggleModal={e => this.setState({ isAddTopic: e })}
            type={this.state.addTopicActionType}
            createTopics={e => this.addEditModalAction(e)}
            topicData={this.state.editTopicData}
            allCategories={allCategories}
            resetAddTopicForm={this.props.topics.createTopicsSuccess}
            createTopicsBeginStatus={this.props.topics.createTopicsBegin}
            updateTopicsBegin={this.props.topics.updateTopicsBegin}
          />
        )}
        {confirmDeleteModal && (
          <DeleteConfirmation
            show={confirmDeleteModal}
            title="Delete Category or Topic"
            disabled={deleteTopicBegin}
            message={
              topicToDelete != null
                ? `You are deleting <strong>'${topicToDelete.topicName}'</strong>. This action cannot be undone. Proceed?`
                : ''
            }
            toggleModal={() => this.toggleModal()}
            confirmDelete={() => this.deleteTopic()}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    topics: state.topics,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions, updateTopicDropdownOnAddTopic }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Topics);

