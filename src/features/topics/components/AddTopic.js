import React, { Component } from 'react';
import { TagsInput, Modal, AutoComplete } from '../../shared-components';

const initialState = {
  topicName: '',
  topicKeywords: [],
  description: '',
  categories: [],
  excludedKeywords: [],
  status: false,
};

const maxTitleLength = 50;
class AddTopic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: {},
      topicData: JSON.parse(JSON.stringify(initialState)),
    };
  }

  formSubmit = event => {
    event.preventDefault();
    const { topicData } = this.state;
    const { createTopics } = this.props;
    if (this.validateAddTopicForm()) {
      return false;
    } else {
      createTopics(topicData);
    }
  };

  validateAddTopicForm = () => {
    let formError = false;
    let { error, topicData } = this.state;
    if (!topicData.topicName) {
      error['topicName'] = true;
      formError = true;
      this.setState({ error });
    }
    if (!topicData.topicKeywords.length) {
      error['topicKeywords'] = true;
      formError = true;
      this.setState({ error });
    }
    if (topicData.description.length > 150) {
      error['description'] = true;
      formError = true;
      this.setState({ error });
    }
    if (formError) return formError;
    if (
      topicData.topicName &&
      topicData.topicKeywords.length &&
      topicData.description.length < 150
    ) {
      this.setState({ error: {} });
    }
    return formError;
  };

  selectedTags = (value, key) => {
    let { topicData, error } = this.state;
    topicData[key] = value;
    if (value.length) delete error[key];
    this.setState({ topicData, error });
  };

  setFormValues = event => {
    let { topicData, error } = this.state;
    if (event.target.name === 'description' && event.target.value.length >= 150) {
      event.target.value.slice(0, 150);
      return false;
    }
    topicData[event.target.name] = event.target.value;
    delete error[event.target.name];
    this.setState({
      error,
      topicData,
    });
  };

  componentWillUnmount() {
    this.setState({ topicData: JSON.parse(JSON.stringify(initialState)) });
    document.body.classList.remove('modal-open');
  }
  componentDidMount() {
    if (this.props.show) {
      document.body.classList.add('modal-open');
    }
    if (this.props.type === 'edit') {
      this.setState({
        topicData: JSON.parse(JSON.stringify(this.props.topicData)),
      });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      JSON.stringify(nextProps.topicData) !== JSON.stringify(this.state.topicData) &&
      nextProps.type === 'edit'
    ) {
      this.setState({
        topicData: nextProps.topicData,
      });
    }
    if (nextProps.type === 'add') {
      this.setState({ topicData: JSON.parse(JSON.stringify(initialState)), error: {} });
    }
    if (nextProps.resetAddTopicForm) {
      this.setState({ topicData: JSON.parse(JSON.stringify(initialState)), error: {} });
    }
  }

  render() {
    const {
      toggleModal,
      type,
      show,
      createTopicsBeginStatus,
      allCategories,
      updateTopicsBegin,
    } = this.props;
    const { topicData, error } = this.state;
    return (
      <Modal handleClick={() => toggleModal(false)}>
        <div className={`modal add-topics ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">{type === 'add' ? 'Add Topic' : 'Edit Topic'}</h4>
                <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
              </div>
              <form noValidate onSubmit={e => this.formSubmit(e)}>
                <div className="modal-body">
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Topic Name*</label>
                    <div className="col-sm-8 topic-input-group">
                      <input
                        type="text"
                        maxLength={maxTitleLength}
                        className={`form-control add-topic-input ${
                          error && error.topicName ? 'input-error' : ''
                        }`}
                        defaultValue={topicData.topicName}
                        name="topicName"
                        onChange={e => this.setFormValues(e)}
                      />
                    </div>
                    {error && error.topicName ? (
                      <span className="instruction-msg error">Please enter a topic name</span>
                    ) : (
                      <span className="instruction-msg">Enter a topic name</span>
                    )}
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Description</label>
                    <div className="col-sm-8 topic-input-group">
                      <textarea
                        className="form-control add-topic-input"
                        value={topicData.description}
                        name="description"
                        onChange={e => this.setFormValues(e)}
                      ></textarea>
                    </div>
                    <span className="instruction-msg">Maximum 150 characters</span>
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Topic Keywords*</label>
                    <div className="col-sm-8 topic-input-group">
                      <div
                        className={`add-topic-input ${
                          error && error.topicKeywords ? 'input-error' : ''
                        }`}
                      >
                        <TagsInput
                          placeholder="Press comma to add topics"
                          selectedTags={e => this.selectedTags(e, 'topicKeywords')}
                          tagData={topicData['topicKeywords']}
                        />
                      </div>
                    </div>
                    {error && error.topicKeywords ? (
                      <span className="instruction-msg error">
                        Please enter a topic keywords and separate them with commas.
                      </span>
                    ) : (
                      <span className="instruction-msg">
                        Enter a topic keywords and separate them with commas
                      </span>
                    )}
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Categories</label>
                    <div className="col-sm-8 topic-input-group">
                      <div className="add-topic-input">
                        <AutoComplete
                          suggestions={allCategories}
                          placeholder="Please add category"
                          selectedTags={e => this.selectedTags(e, 'categories')}
                          tagData={topicData['categories']}
                        />
                      </div>
                    </div>
                    <span className="instruction-msg">
                      Enter a category or select one from the list
                    </span>
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Exclude Keywords</label>
                    <div className="col-sm-8 topic-input-group">
                      <div className="add-topic-input">
                        <TagsInput
                          placeholder="Press comma to add tags"
                          selectedTags={e => this.selectedTags(e, 'excludeKeywords')}
                          tagData={topicData['excludeKeywords']}
                        />
                      </div>
                    </div>
                    <span className="instruction-msg">
                      Enter a keyword or phrase and separate them with commas.
                    </span>
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label mandatory-msg">
                      * Denotes mandatory field.
                    </label>
                  </div>
                </div>

                <div className="modal-footer">
                  <button
                    type="button"
                    className={`btn ${
                      createTopicsBeginStatus || updateTopicsBegin ? 'disabled' : ''
                    }`}
                    onClick={() => toggleModal(false)}
                  >
                    CANCEL
                  </button>
                  <button
                    type="submit"
                    className={`btn btn-submit ${
                      createTopicsBeginStatus || updateTopicsBegin ? 'disabled' : ''
                    }`}
                  >
                    {(createTopicsBeginStatus || updateTopicsBegin) && (
                      <i className="la la-spinner la-spin" />
                    )}
                    {type === 'add' ? 'ADD' : 'SAVE CHANGES'}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

export default AddTopic;
