import initialState from './initialState';
import { reducer as retrieveTopics } from './retrieveTopics';
import { reducer as createTopic } from './createTopic';
import { reducer as retrieveDelete } from './deleteTopic';
import { reducer as categoriesReducer } from './retrieveAllCategories';

const reducers = [retrieveTopics, createTopic, retrieveDelete, categoriesReducer];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case 'RESET_TOPICS_STATE':
      return {
        ...state,
        ...initialState,
      };
    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
