import { DELETE_TOPIC_BEGIN, DELETE_TOPIC_SUCCESS, DELETE_TOPIC_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function deleteTopic(props = {}) {
  return dispatch => {
    dispatch({
      type: DELETE_TOPIC_BEGIN,
    });
    const url = `${apiEndPoints.topics.DELETE_TOPIC}?topicId=${props.topicId}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.delete(url);
      doRequest.then(
        res => {
          dispatch({
            type: DELETE_TOPIC_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: DELETE_TOPIC_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case DELETE_TOPIC_BEGIN:
      return {
        ...state,
        deleteTopicBegin: true,
        deleteTopicdFailure: false,
        deleteTopicSuccess: false,
      };
    case DELETE_TOPIC_SUCCESS:
      return {
        ...state,
        deleteTopicBegin: false,
        deleteTopicSuccess: true,
      };
    case DELETE_TOPIC_FAILURE:
      return {
        ...state,
        deleteTopicBegin: false,
        deleteTopicdFailure: true,
      };

    default:
      return state;
  }
}
