import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import * as updateSource from '../home/redux/updateSourcesFilter';
import { toast } from 'react-toastify';
import { Loader } from '../shared-components';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { socialIcons } from '../../styles/icons';
import { FACEBOOK_APP_ID, FACEBOOK_SOCIAL_ICOSNS_KEY, FACEBOOK_CONNECT_SCOPE } from './redux/constants';

class Input extends Component {
  render() {
    return <input {...this.props} />;
  }
}
export class Sources extends Component {
  static propTypes = {
    sources: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      allSources: [],
      isSourceUpdating: false,
      isFacebookDisconnecting: false,
      isFacebookConnected: false,
      facebookUserId: null,
      facebookAccessToken: null
    };
  }


  handleResponseFromFacebookConnect = (response) => {
    if (response.id && response.accessToken) {
      toast.success(' Successfully connected to Facebook.');
      this.setState({
        isFacebookConnected: true,
        facebookUserId: response.id,
        facebookAccessToken: response.accessToken
      }, () => {
        this._saveFacebookAuthInfosFromStateIntoLocalStorage();
      });
    }
    else {
      toast.error(" Error while trying to connect to Facebook.");
    }
  }

  handleDisconnectFacebook = () => {
    this.setState({
      isFacebookDisconnecting: true
    }, () => {
      this.props.actions.disconnectFacebook({
        facebookAccessToken: this.state.facebookAccessToken,
        facebookUserId: this.state.facebookUserId
      }, () => {
        this._saveFacebookAuthInfosFromStateIntoLocalStorage();
      });
    });
  }
  
  _saveFacebookAuthInfosFromStateIntoLocalStorage() {
    const sourcesAuthInfoKey = 'sourcesAuthInfo';
    let sourcesAuthInfo = JSON.parse(localStorage.getItem(sourcesAuthInfoKey));
    if (! sourcesAuthInfo) {
      sourcesAuthInfo = {};
    }
    sourcesAuthInfo.isFacebookConnected = this.state.isFacebookConnected;
    sourcesAuthInfo.facebookUserId = this.state.facebookUserId;
    sourcesAuthInfo.facebookAccessToken = this.state.facebookAccessToken;
    localStorage.setItem(sourcesAuthInfoKey,JSON.stringify(sourcesAuthInfo));
  }

  _loadFacebookAuthInfoFromLocalStorageIntoState() {
    const sourcesAuthInfoKey = 'sourcesAuthInfo';
    let sourcesAuthInfo = JSON.parse(localStorage.getItem(sourcesAuthInfoKey));
    if (sourcesAuthInfo && sourcesAuthInfo.isFacebookConnected && sourcesAuthInfo.facebookUserId && sourcesAuthInfo.facebookAccessToken) {
      this.setState({
        isFacebookConnected: sourcesAuthInfo.isFacebookConnected,
        facebookUserId: sourcesAuthInfo.facebookUserId,
        facebookAccessToken: sourcesAuthInfo.facebookAccessToken
      });
    }
  }

  setSourcesStatus = sourceName => {
    const { allSources } = this.state;
    const updateSource = allSources.filter(source => source.sourceName === sourceName)[0];
    const updatedSource = allSources.map(source => {
      if (source.sourceName.toLowerCase() === sourceName.toLowerCase())
        source.selected = !source.selected;
      return source;
    });
    this.setState(
      {
        allSources: [...updatedSource],
        isSourceUpdating: true,
      },
      () => {
        this.props.actions.updateSelectedSourceStatus(updateSource);
      },
    );
  };

  navigateBack = () => {
    this.props.history.goBack();
  };
  componentDidMount() {
    const { retrieveSources } = this.props.actions;
    retrieveSources();
    this._loadFacebookAuthInfoFromLocalStorageIntoState();
  }
  componentWillUnmount() {
    this.setState({
      allSources: [],
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    if (
      props.sources.allAvailableSources.length &&
      !this.state.isSourceUpdating &&
      JSON.stringify(props.sources.allAvailableSources) !== JSON.stringify(this.state.allSources)
    ) {
      props.actions.updateAvailableSources(props.sources.allAvailableSources);
      this.setState({ allSources: [...props.sources.allAvailableSources] });
    }
    if (props.sources.update_sources_success && this.state.isSourceUpdating) {
      toast.success(' Sources selection has been updated!');
      this.setState({
        isSourceUpdating: false,
      });
    }
    if (props.sources.update_sources_error && this.state.isSourceUpdating) {
      toast.error(' Sources selection has not been updated!');
      this.setState({
        isSourceUpdating: false,
      });
    }
    this._setEmptyFacebookAuthInfoIfFacebookDisconnected(props);
    // return null;
  }

  _setEmptyFacebookAuthInfoIfFacebookDisconnected(props) {
    if (this.state.isFacebookDisconnecting) {
      // This request may fail if the API token was already invalidated by some
      // other means or for some other reason.
      //
      // The safest assumption is to assume Facebook is disconnected even if 
      // an error is received.
      //
      // The user then has an opportunity to re-connect if needed. Otherwise, 
      // the user would be stuck with a disconnected Facebook account that 
      // we display as still connected.  
      if (props.sources.disconnect_facebook_success || props.sources.disconnect_facebook_error) {
        if (props.sources.disconnect_facebook_success) {
          toast.success(' Facebook successfully disconnected.');
        }
        this.setState({
          isFacebookConnected: false,
          facebookUserId: null,
          facebookAccessToken: null,
          isFacebookDisconnecting: false
        }, () => {
          this._saveFacebookAuthInfosFromStateIntoLocalStorage();
        });
      }
    }
  }


  render() {
    let allSources = [];
    allSources.push({
      sourceType: 'Social Media',
      sourcesData: this.state.allSources.filter(
        source => source.sourceType.toLowerCase() === 'social media',
      ),
    });
    allSources.push({
      sourceType: 'News',
      sourcesData: this.state.allSources.filter(
        source => source.sourceType.toLowerCase() === 'news',
      ),
    });
    const {isFacebookConnected} = this.state;
    return (
      <div className="sources-container">
        <div className="main-container">
          {allSources.length > 0 &&
            allSources.map((sources, i) => (
              <div className="col-6 sub-container" key={i}>
                <div className="col-12 source-content">
                  <div className="col-9 source-inner-content">
                    <h6 className="sub-text">{sources.sourceType}</h6>
                    {!sources.sourcesData.length && <Loader />}
                    {sources.sourcesData.length > 0 &&
                      sources.sourcesData.map((sourceData, index) => (
                        <div className="select-item" key={index}>
                          <Input
                            className="styled-checkbox"
                            id={sourceData.sourceName}
                            type="checkbox"
                            onChange={() => this.setSourcesStatus(sourceData.sourceName)}
                            checked={sourceData.selected}
                            value={sourceData.sourceName}
                          />
                          <label htmlFor={sourceData.sourceName}>{sourceData.sourceName}</label>
                          {sourceData.sourceType !== 'news' && sourceData.sourceName !== "facebook" && (
                            <div className="">
                              <div className="connection-status">
                                <span className="manage-connection">
                                  <span className="connection-btn">(Connect)</span>
                                </span>
                              </div>
                            </div>
                          )}
                          {sourceData.sourceName === "facebook" && (
                            <div className="">
                              <div className="connection-status">
                                <span className="manage-connection">
                                    {isFacebookConnected ? (
                                        <span onClick={this.handleDisconnectFacebook} className="facebook-connection-btn">
                                          <span
                                            dangerouslySetInnerHTML={{
                                              __html: socialIcons[FACEBOOK_SOCIAL_ICOSNS_KEY]
                                            }}
                                          /> 
                                            (Disconnect)
                                          </span>
                                      ) : (
                                        <FacebookLogin
                                          appId={FACEBOOK_APP_ID}
                                          autoLoad={false}
                                          scope={FACEBOOK_CONNECT_SCOPE}
                                          callback={this.handleResponseFromFacebookConnect}
                                          render={renderProps => (
                                            <span  onClick={renderProps.onClick} className="facebook-connection-btn">
                                              <span
                                                dangerouslySetInnerHTML={{
                                                  __html: socialIcons[FACEBOOK_SOCIAL_ICOSNS_KEY]
                                                }}
                                              /> 
                                              (Connect)
                                            </span>
                                          )}
                                        />
                                      )}
                                  </span>
                                </div>
                              </div>
                            )}
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    sources: state.sources,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions, ...updateSource }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Sources);
