const initialState = {
  retrieve_sources_begin: false,
  retrieve_sources_error: false,
  retrieve_sources_success: false,
  allAvailableSources: [],
  update_sources_begin: false,
  update_sources_success: false,
  update_sources_error: false,
};

export default initialState;
