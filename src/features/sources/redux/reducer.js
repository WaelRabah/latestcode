import initialState from './initialState';
import { reducer as retrieveSources } from './retrieveSources';
import { reducer as updateSources } from './updateSources';
import { reducer as disconnectFacebook } from './disconnectFacebook';


const reducers = [retrieveSources, updateSources, disconnectFacebook];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
