import { DISCONNECT_FACEBOOK_BEGIN, DISCONNECT_FACEBOOK_SUCCESS, DISCONNECT_FACEBOOK_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function disconnectFacebook(props = {}) {
  return dispatch => {
    dispatch({
      type: DISCONNECT_FACEBOOK_BEGIN,
      data: props,
    });
    const url = `${apiEndPoints.facebook.ROOT_URL}/${props.facebookUserId}/permissions?access_token=${props.facebookAccessToken}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.delete(url);
      doRequest.then(
        res => {
          dispatch({
            type: DISCONNECT_FACEBOOK_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: DISCONNECT_FACEBOOK_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case DISCONNECT_FACEBOOK_BEGIN:
      return {
        ...state,
        disconnect_facebook_begin: true,
        disconnect_facebook_success: false,
        disconnect_facebook_error: false,
      };
    case DISCONNECT_FACEBOOK_SUCCESS:
      return {
        ...state,
        disconnect_facebook_begin: false,
        disconnect_facebook_success: true,
        disconnect_facebook_error: false,
      };
    case DISCONNECT_FACEBOOK_FAILURE:
      return {
        ...state,
        disconnect_facebook_begin: false,
        disconnect_facebook_success: false,
        disconnect_facebook_error: true,
      };

    default:
      return state;
  }
}
