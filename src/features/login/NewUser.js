import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { toast } from 'react-toastify';

const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
const initialState = {
  name: '',
  email: '',
  message: '',
};
class NewUser extends Component {
  static propTypes = {
    newUser: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      error: {},
      sendMessage: false,
    };
    this.setFormValue = this.setFormValue.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
  }

  componentDidMount() {
    if (this.props.show) {
      document.body.classList.add('modal-open');
    }
  }

  componentWillUnmount() {
    document.body.classList.remove('modal-open');
  }
  setFormValue = event => {
    let { error } = this.state;
    delete error[event.target.name];
    this.setState({
      [event.target.name]: event.target.value,
      error,
    });
  };

  validateEmail = event => {
    const email = event.target.value;
    if (email.length) {
      if (!reg.test(email)) {
        let { error } = this.state;
        error[event.target.name] = true;
        this.setState({
          error,
        });
      }
    }
  };

  UNSAFE_componentWillReceiveProps(newProps) {
    if (newProps.newUser.sendMessageSuccess && this.state.sendMessage) {
      toast.success('Message send successfully.');
      this.setState(
        {
          sendMessage: false,
        },
        () => {
          this.props.toggleModal(false);
        },
      );
    }
    if (newProps.newUser.sendMessageFailure && this.state.sendMessage) {
      toast.error('Something went wrong!');
      this.setState({
        sendMessage: false,
      });
    }
  }

  submitForm = () => {
    const { email, message, name } = this.state;
    const { sendMessage } = this.props.actions;
    let { error } = this.state;
    let result = false;
    Object.keys(this.state).forEach(formField => {
      if (['email', 'message', 'name'].indexOf(formField) != -1 && !this.state[formField]) {
        error[formField] = true;
        result = true;
      }
    });
    if (result) {
      this.setState({ error });
      return false;
    }
    if (!reg.test(email)) {
      error['email'] = true;
      this.setState({
        error,
      });
      return false;
    }
    const postData = {
      email,
      message,
      name,
    };
    this.setState(
      {
        sendMessage: true,
      },
      () => {
        sendMessage(postData);
      },
    );
  };

  render() {
    const { toggleModal, show } = this.props;
    const { name, email, message, error } = this.state;
    const { sendMessageBegin } = this.props.newUser;
    return (
      <div className={`modal add-topics ${show ? 'modal-show' : 'modal-hide'}`}>
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header user-new">
              <h4 className="modal-title">Contact Us</h4>
              <i
                className="la la-close modal-close user-close"
                onClick={() => toggleModal(false)}
              />
            </div>
            <div className="modal-body-new-user">
              <form noValidate>
                <p className="user-message">
                  Send us a message and we'll respond as soon as we can.
                </p>
                <div className="form-container">
                  <div className="form-group new-user">
                    <label>Name</label>
                    <div className="input-field">
                      <input
                        className={`user-input ${error.name && 'input-error'}`}
                        type="text"
                        name="name"
                        onChange={this.setFormValue}
                        defaultValue={name}
                        maxLength="100"
                      />
                      {error && error.name && <p className="error-msg">Please enter your name</p>}
                    </div>
                  </div>

                  <div className="form-group new-user">
                    <label>Email Address</label>
                    <div className="input-field">
                      <input
                        className={`user-input top ${error.email && 'input-error'}`}
                        type="email"
                        name="email"
                        defaultValue={email}
                        onBlur={this.validateEmail}
                        onChange={this.setFormValue}
                      />
                      {error && error.email && (
                        <p className="error-msg">Please enter a valid email address</p>
                      )}
                    </div>
                  </div>

                  <div className="form-group new-user">
                    <label>Message</label>
                    <div className="input-field">
                      <textarea
                        className={`user-input high ${error.message && 'input-error'}`}
                        type="text"
                        maxLength="4000"
                        name="message"
                        defaultValue={message}
                        onChange={this.setFormValue}
                      ></textarea>
                      {error && error.message ? (
                        <p className="error-msg">
                          Share with us any information that might help us to respond to your
                          request
                        </p>
                      ) : (
                        <p>Maximum 4000 characters</p>
                      )}
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div className="modal-footer btn">
              <button
                type="button"
                className={`btn ${sendMessageBegin ? 'disabled' : ''}`}
                onClick={() => toggleModal(false)}
              >
                CANCEL
              </button>
              <button
                type="submit"
                className={`btn ${sendMessageBegin ? 'disabled' : ''} btn-submit`}
                onClick={() => this.submitForm()}
              >
                {sendMessageBegin ? (
                  <React.Fragment>
                    <i className="la la-spinner la-spin" /> SENDING
                  </React.Fragment>
                ) : (
                  'SEND'
                )}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newUser: state.newUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewUser);
