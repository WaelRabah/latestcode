import { Reports } from './';

export default {
  path: '/dashboard/reports',
  name: 'Reports',
  childRoutes: [{ path: 'reports', name: 'Reports', component: Reports, isIndex: true }],
};
